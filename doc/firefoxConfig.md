You can modify the browser configuration by open a page with the URL _about:config_ . 

| Parameter                     | Value  | Description                                                 |
|:------------------------------|:------:|:------------------------------------------------------------|
| devtools.hud.loglimit.console | 200000 |We need to extend the maximum amount of shown entries in the developer console because else you may miss the log entries of the test execution. |
