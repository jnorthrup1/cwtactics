package org.wolftec.cwt.test.base;

import org.stjs.javascript.annotation.SyntheticType;

@SyntheticType
public class TestMethodResult
{
  public String name;
  public boolean succeeded;
  public Exception error;
}