package org.wolftec.cwt.test.base;

import org.stjs.javascript.Array;
import org.stjs.javascript.annotation.SyntheticType;

@SyntheticType
public class TestClassResult
{
  public String name;
  public Array<TestMethodResult> methods;
}