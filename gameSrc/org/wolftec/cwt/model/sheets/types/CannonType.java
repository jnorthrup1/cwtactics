package org.wolftec.cwt.model.sheets.types;

public class CannonType
{
  public int range;
  public int damage;
  public String direction;
}
