package org.wolftec.cwt.model.gameround;

public interface Ownable
{
  Player getOwner();
}
