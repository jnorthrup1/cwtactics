package org.wolftec.cwt.states.base;

public interface GameActions
{

  public static final String BUTTON_A = "A";
  public static final String BUTTON_B = "B";
  public static final String BUTTON_LEFT = "LEFT";
  public static final String BUTTON_RIGHT = "RIGHT";
  public static final String BUTTON_UP = "UP";
  public static final String BUTTON_DOWN = "DOWN";
}
