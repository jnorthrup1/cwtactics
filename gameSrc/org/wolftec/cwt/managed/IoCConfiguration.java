package org.wolftec.cwt.managed;

import org.stjs.javascript.Array;
import org.stjs.javascript.annotation.SyntheticType;

@SyntheticType
public class IoCConfiguration
{

  public Array<String> namespaces;
}
