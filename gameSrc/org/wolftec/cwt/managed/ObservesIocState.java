package org.wolftec.cwt.managed;

public interface ObservesIocState
{

  default void onIocReady()
  {
  }
}
