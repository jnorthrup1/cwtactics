package org.wolftec.cwt.managed;

public interface ManagedClass
{

  default void onConstruction()
  {
  }
}
