/**
 * This package contains the game custom wars tactics.
 */
@org.stjs.javascript.annotation.Namespace(org.wolftec.cwt.Constants.NAMESPACE)
package org.wolftec.cwt;