package org.wolftec.cwt.tags;

/**
 * This is a marker interface to mark managed classes with {@link Configuration}
 * properties.
 */
public interface Configurable
{

}
