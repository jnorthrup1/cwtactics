package org.wolftec.cwt.action;

public enum TileMeta
{
  EMPTY, OWN, OWN_USED, ALLIED, ENEMY, NEUTRAL
}
