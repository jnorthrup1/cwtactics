package org.wolftec.cwt.action;

public enum MoveMeta
{
  SET_POSITION, PREVENT_CLEAR_OLD_POS, PREVENT_SET_NEW_POS;
}
