package org.wolftec.cwt.input;

interface InputBackend
{

  /**
   * Enables the object.
   */
  void enable();

  /**
   * Disables the object.
   */
  void disable();
}