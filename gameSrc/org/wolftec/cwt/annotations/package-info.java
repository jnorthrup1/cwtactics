/**
 * This package contains some documentation annotations. This means this
 * annotations won't be generated in the result javaScript code nor changes
 * anything in your code. They are simply to extend the documentation or methods
 * and parameters (especially when you use IDE's like Eclipse).
 */
package org.wolftec.cwt.annotations;