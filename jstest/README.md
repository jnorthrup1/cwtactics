Well, it is a start. But to run this, you'll need to point the server at this folder.

Then, you'll want to run imgLibrary.html from localhost.

Image List:

YellowComet.jpg
AWDS_INFT.png
CWT_BASE.png
CWT_MECH.png
CWTitle1.png
CWTitle2.png
day.png
lightning bolt.png
MinuteWars.png
night.png
UnitBaseColors.png

After that, you'll see buttons:

The text box - Where you put in an image name (you can put your own if relative location)
Store - Stores an image into memory
Display - Displays the last image stored in memory
Cascade - Cascades all the images stored in memory (also shows frames per second)
Display # - Keeps a record of all images stored in memory, can be use to display an older image

This is a living test, and will keep expanding. So far, works in all browsers though IE continues to ran really slowly at around half the rate of the other browsers.

